package com.hl.tools.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by 31464 on 2021/10/20.
 */
public class SendSystemEmailUtils {


    /**
     * 邮件发送
     * @param emailUrl  邮件地址
     * @param destAddress  发送目的地
     * @param ccAddress
     * @param bccAddress
     * @param subject  主题
     * @param body  内容
     * @return
     */
    public static String sendSysEmail(String emailUrl,String destAddress,String ccAddress,String bccAddress,String subject,String body){
        String txtAccessToken=getXAccessToken("");

        String requestUrl=emailUrl;

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPost = new HttpPost(requestUrl);
        httpPost.setHeader("X-Acess-Token",txtAccessToken);
        httpPost.setHeader("Content-Type","application/json");
        JSONObject jsonObject=new JSONObject();

        jsonObject.put("destAddress",destAddress);
        jsonObject.put("ccAddress",ccAddress);
        jsonObject.put("bccAdress",bccAddress);
        jsonObject.put("subject",subject);
        jsonObject.put("body",body);

        StringEntity stringEntity = new StringEntity(jsonObject.toString(), "utf-8");

        stringEntity.setContentEncoding("UTF-8");

        CloseableHttpResponse response=null;

        String result=null;
        httpPost.setEntity(stringEntity);
        try {
            response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());

            if(StringUtils.isNoneBlank(result)){
                result=JSON.parseObject(result).getString("emailId");
            }

            if(HttpStatus.SC_OK==response.getStatusLine().getStatusCode()){
                System.out.println("emailId："+result);
            }else{
                System.out.println(result);
            }
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }



    private static String getXAccessToken(String tokenAccessMsg){
        CloseableHttpClient httpClient= HttpClients.createDefault();

        HttpGet httpGet = new HttpGet(tokenAccessMsg);
        CloseableHttpResponse response=null;
        String result=null;
        String code=null;
        try {
            response = httpClient.execute(httpGet);
            result= EntityUtils.toString(response.getEntity());

            if(HttpStatus.SC_OK == response.getStatusLine().getStatusCode()){
                JSONObject jsonObject = JSONObject.parseObject(result);
                code=jsonObject.getString("accessToken");
                response.close();
            }else{
                response.close();
                throw new Exception(result);
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
        return code;
    }
}
