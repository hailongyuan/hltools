package com.hl.tools.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigUtil;

public class ConfigUtils {
    private Config config;
    public ConfigUtils(){
         config = ConfigFactory.load("application1.properties");
    }


    public Config getConfig(){
        return config;
    }


    public static void main(String[] args) {
        ConfigUtils configUtils=new ConfigUtils();

        String string = configUtils.getConfig().getString("application.name");

        System.out.println(string);
    }


}
