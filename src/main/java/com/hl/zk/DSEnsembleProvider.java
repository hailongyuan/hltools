package com.hl.zk;

import org.apache.curator.ensemble.EnsembleProvider;

import java.io.IOException;

public class DSEnsembleProvider implements EnsembleProvider {

    private final String serverList;

    public DSEnsembleProvider(String serverList){
        this.serverList=serverList;
    }

    @Override
    public void start() throws Exception {

    }

    @Override
    public String getConnectionString() {
        return this.serverList;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public void setConnectionString(String s) {

    }

    @Override
    public boolean updateServerListEnabled() {
        return false;
    }
}
