package com.hl.zk;

import org.apache.curator.ensemble.EnsembleProvider;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.io.IOException;

public class ZookpeerOperator {

    private ZookeeperConfig zookeeperConfig;

    public CuratorFramework zkClient;


    public ZookpeerOperator(){
        init();
        this.zkClient=buildZKClient();
    }


    public void init(){
        zookeeperConfig=new ZookeeperConfig();
    }



    public CuratorFramework buildZKClient(){
        CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder()
                .ensembleProvider(new DSEnsembleProvider(zookeeperConfig.getServerList()))
                .retryPolicy(new ExponentialBackoffRetry(zookeeperConfig.getBaseSleepTimeMs(), zookeeperConfig.getMaxRetries(), zookeeperConfig.getBaseSleepTimeMs()));
        this.zkClient=builder.build();
        this.zkClient.start();

        try {
            this.zkClient.blockUntilConnected();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return this.zkClient;
    }




}
