package com.hl.zk;

public class ZookeeperConfig {
    private String serverList="localhost:2181";
    private int baseSleepTimeMs=100;
    private int maxSleepMs=30000;
    private int maxRetries=10;
    private String dsRoot="/hailongds";


    public String getServerList() {
        return serverList;
    }

    public void setServerList(String serverList) {
        this.serverList = serverList;
    }

    public int getBaseSleepTimeMs() {
        return baseSleepTimeMs;
    }

    public void setBaseSleepTimeMs(int baseSleepTimeMs) {
        this.baseSleepTimeMs = baseSleepTimeMs;
    }

    public int getMaxSleepMs() {
        return maxSleepMs;
    }

    public void setMaxSleepMs(int maxSleepMs) {
        this.maxSleepMs = maxSleepMs;
    }

    public int getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public String getDsRoot() {
        return dsRoot;
    }

    public void setDsRoot(String dsRoot) {
        this.dsRoot = dsRoot;
    }
}
