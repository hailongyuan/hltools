package com.hl.netty;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.Date;

public class MyNettyServerHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println(new Date()+"   :服务端收到消息:"+(String)msg+"   ");

        ctx.writeAndFlush("hello i netty server");
                //.addListener(ChannelFutureListener.CLOSE);
        ctx.fireChannelRead(msg);

    }
}
