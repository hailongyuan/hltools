package com.hl.netty;


import io.netty.channel.Channel;

public class ChannelSubmitTaskRunnable implements Runnable{

    private Channel channel;
    public ChannelSubmitTaskRunnable(Channel channel){
        this.channel=channel;
    }

    @Override
    public void run() {
        System.out.println("ChannelSubmitTaskRunnable Start....");

        int index=1;
        while (true){
            sleep();
            this.channel.writeAndFlush(" client msg index:"+ index++);
        }

    }


    public void sleep(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
