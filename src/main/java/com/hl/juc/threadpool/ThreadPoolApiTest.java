package com.hl.juc.threadpool;

import java.util.concurrent.*;

public class ThreadPoolApiTest {

    private static ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(100, 100,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>());


    public static void main(String[] args) {
        threadPoolExecutor.submit(new ThreadPoolRunnable());
        threadPoolExecutor.submit(new ThreadPoolRunnable());
        System.out.println(threadPoolExecutor.getActiveCount());

    }

}

class ThreadPoolRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println("Thread-Name:"+Thread.currentThread().getName());
    }
}
