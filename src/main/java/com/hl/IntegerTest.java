package com.hl;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by yuanhailong on 2022/9/7.
 */
public class IntegerTest {
    public static void main(String[] args) throws Exception {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(strToInt(Integer.MAX_VALUE+""));

    }


    public static int strToInt(String str) throws Exception {
        int retVal=0;
        int radix=10;
        if(StringUtils.isEmpty(str)){
            throw new EmptyException();
        }

        int strLen=str.length();

        char firstChar=str.charAt(0);
        boolean flag=false;
        int point=0;
        if(firstChar<'0'){
            if(firstChar == '-'){
                flag=true;
                point++;
            }else if(firstChar != '+'){
                throw new NumberFormatException();
            }

            if(strLen==1){
                throw  new NumberFormatException();
            }
        }


        while (point<strLen){
            int digit = Character.digit(str.charAt(point++), radix);

            if(digit<0){
                throw new NumberFormatException();
            }

            retVal *= radix;


            retVal +=digit;

        }



        return flag?-retVal:retVal;


    }



}

class EmptyException extends Exception{
    public EmptyException(){
        super("data is empty");
    }
}
