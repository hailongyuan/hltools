package com.hl.algor_data_struct.sort;

/**
 * Created by yuanhailong on 2021/10/24.
 *
 */
public class BinarySearchNumExists {

    static int arr[]={1,3,4,5,9,20,44,100,201};


    //在一个有序数组中查看某个数组是否存在    二分查找方法
    public static void binarySearch(int num){
        int rightIndex=0;
        int leftIndex=0;
        int index=arr.length/2;
        if(arr[index]==num){
            System.out.println("存在");
        }else if(num<arr[index]){
            leftIndex=0;
            rightIndex=index;
        }else{
            leftIndex=index;
            rightIndex=arr.length-1;
        }





    }


    //在一个无序的数组中且任何相邻的两个数不相等，求局部最小值
    //局部最小的概念  如果  0位置小于1位置 则直接返回0（头部比较）  如果   N-2 位置 小于N-1位置 则直接返回N-2(尾部比较)
    //如果   M+1 大于 M 且M-1 小于 M 则 返回M
    //时间复杂度需要优于O(N)
}
