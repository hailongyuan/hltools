package com.hl.algor_data_struct.sort;

/**
 * Created by yuanhailong on 2021/10/23.
 *
 * 剥析递归和递归时间复杂度的估算
 *  用递归方法找数组中最大值，系统上到底是怎么做的。
 *   master公式的使用
 *       T(N)=a*T(N/b) +O(N^d)
 *
 */
public class SortConstants {
    public static int[] sortArr={23, 6, 28, 7, 1, 9, 10, 48, 20};


    /**
     * 元素位置相互交换
     *
     * 异或运算性质 ：
     *
     *   1 相同为0 ，不同为一
     *   2 0和任何数异或N 等于 N  ，  0^N=N
     *   3 任何数和自己异或得到0      N^N=0
     *   4 异或运算 满足交换律和结合律
     *
     *
     * @param arr  原始数组
     * @param i
     * @param j
     */
    public static void swap(int[] arr,int i,int j){
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

}
