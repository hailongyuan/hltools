package com.hl.algor_data_struct.sort;

/**
 * Created by yuanhailong on 2021/10/24.
 *
 * 求一个数组中最大值
 */
public class GetMaxInArr {

    /**
     * 用递归的方式做
     * @param arr
     * @param l
     * @param r
     * @return
     */
    public static int process(int arr[],int l,int r){
        if(l==r){  //表示只有一个元素，直接返回即可
            return arr[l];
        }

        //获取中间位置
        //一般获取数组中间位置用(l+r)/2  但是这样有一个缺陷，就是当l和r都很大的时，l+r可能会溢出。导致结果错误
        //l+((r-l)>>1) 等同于 l+(r-l)/2 只不过这里用了位运算，位运算比算数运算效率要高
        int mid=l+((r-l)>>1);

        int leftMax=process(arr,l,mid);
        int rightMax=process(arr,mid+1,r);

        return Math.max(leftMax,rightMax);

    }


    public static void main(String[] args) {
        int process = process(SortConstants.sortArr, 0, SortConstants.sortArr.length - 1);
        System.out.println(process);
    }

}
