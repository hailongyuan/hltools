package com.hl.algor_data_struct.sort;

import java.util.Arrays;
import java.util.List;

/**
 * Created by yuanhailong on 2021/10/23.
 * 冒泡排序
 *     每次比较相邻两个元素，如果后面元素的值大于前面元素的值，则相互交换
 *
 *     时间复杂度O(N^2)
 */
public class BubbleSort {
    public static int[] arr={23, 6, 28, 7, 1, 9, 10, 48, 20};
    public static void bubbleSort(int[] arr){
        for(int i=arr.length-1;i>0;i--){
            for(int j=0;j<i;j++){
                //如果相邻元素，前面的大于后面的，则进行交换
                if(arr[j]>arr[j+1]){
                    SortConstants.swap(arr,j,j+1);
                }
            }
        }
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }

    }


    public static void main(String[] args) {

        bubbleSort(SortConstants.sortArr);


    }

}
