package com.hl.algor_data_struct.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by yuanhailong on 2021/10/28.
 */
public class Graph {
    private ArrayList<String> vertexList;  //存储顶点集合
    private int[][] edges;//存储图对应的领接矩阵
    private int numOfEdges;//表示变的数量
    private boolean[] isVisit;


    /**
     *
     * @param n  顶点个数
     */
    public Graph(int n){
        //初始化矩阵和vertexList
        edges=new int[n][n];
        vertexList=new ArrayList<String>(n);
        numOfEdges=0;//边的个数默认先给0
    }


    public void  insertVertex(String vertex){
        vertexList.add(vertex);//在顶点集合列表中插入一个顶点
    }

    /**
     * 添加边
     * @param v1   v1 表示点的下标，即第几个顶点  "A"->"B"   "A"->"0"   "B"->"1"
     * @param v2
     * @param weight
     */
    public void insertEdge(int v1,int v2,int weight){
        edges[v1][v2]=weight;
        edges[v2][v1]=weight;
        numOfEdges++;
    }


    /**
     * 通过下标找到具体的值
     * @param index
     * @return
     */
    public String getValueByIndex(int index){
        return vertexList.get(index);
    }

    /**
     * 查找边的数量
     * @return
     */
    public int getNumOfEdges(){
        return numOfEdges;
    }


    /**
     * 显示矩阵
     */
    public void showGraph(){
        for(int[] link:edges){
            System.out.println(Arrays.toString(link));
        }
    }


    /**
     * 得到第一个邻接节点的下标w
     * @param index 初始节点下标
     * @return 如果存在就返回对应的下标
     */
    public int getFirstNeighbor(int index){
        for(int j=0;j<vertexList.size();j++){
            if(edges[index][j]>0){
                return j;
            }
        }
        return -1;
    }


    /**
     * 根据前一个邻接节点的下标来获取下一个邻接节点
     * 就是在矩阵中找到下一个节点 如下说明
     * 有矩阵
     *    A  B  C  D  E
     * A [0, 1, 1, 0, 0]
     * B [1, 0, 1, 1, 1]
     * C [1, 1, 0, 0, 0]
     * D [0, 1, 0, 0, 0]
     * E [0, 1, 0, 0, 0]
     * 假如在D-A这个查找的数据[D-A在矩阵中为0]找不到，则纵坐标D的位置不变，横坐标向下移动[从A->B] 矩阵位置为D->B
     * @param v1
     * @param v2
     * @return
     */
    public int getNextNeighbor(int v1,int v2){
        for(int j=v2+1;j<vertexList.size();j++){
            if(edges[v1][j]>0){
                return j;
            }
        }
        return -1;
    }


    /**
     * 广度优先基本思想：
     * 类似于一个分层搜索过程，广度优先遍历需要使用一个队列以保持访问过的节点顺序，一边按照这个顺序来访问这些节点的邻接节点
     *
     * 广度优先遍历算法步骤
     * 1) 访问出事节点V并标记节点V为已访问
     * 2) 节点V入对了列
     * 3) 当队列非空时，继续执行，否则算法结束(仅指当前节点V)
     * 4) 出队列，取得队列头结点U
     * 5) 查找节点U的第一个邻接节点W
     * 6) 若节点U的邻接节点W不存在，则转到步骤3,否则循环执行一下三个步骤
     *    6.1 若节点w尚未被访问，则访问节点w并标记为已访问
     *    6.2 节点w入队列
     *    6.3 查找节点u的继w邻接节点后的下一个邻接节点w，转到步骤6
     */
    private void bfs(boolean isVisit[] ,int i){
        int u;  //表示队列头结点对应的下标
        int w;//邻接节点下标w
        //记录已访问节点顺序
        LinkedList<Integer> queue = new LinkedList<>();
        System.out.print(getValueByIndex(i)+"->");

        //标记为已访问
        isVisit[i]=true;
        //入队列
        queue.addLast(i);

        //队列不为空
        while (!queue.isEmpty()){
            //队列头
            u = queue.removeFirst();
            //得到第一个节点的下标w
            w=getFirstNeighbor(u);

            //找到了w节点
            while (w!=-1){
                //w没有访问过
                if(!isVisit[w]){
                    System.out.print(getValueByIndex(w)+"->");
                    //标记为已访问
                    isVisit[w]=true;
                    //入队列
                    queue.addLast(w);
                }
                //w已经访问过了
                //以u为前驱节点，找w后面的下一个邻节点
                w = getNextNeighbor(u, w);  //广度优先
            }
        }
    }


    /**
     * 所有节点都进行广度优先搜索
     */
    public void bfs(){
        isVisit=new boolean[5];
        for(int j=0;j<vertexList.size();j++){
            if(!isVisit[j]){
                bfs(isVisit,j);
            }
        }
    }




    /**
     * 深度优先算法
     * @param isVisited  节点是否被访问的列表
     * @param i  当前访问节点的下标
     */
    public void dfs(boolean[] isVisited,int i){
        //首先输出我们访问的节点
        System.out.print(getValueByIndex(i)+"->");
        //该节点设置为已经访问了
        isVisited[i]=true;
        //得到i节点的下一个节点
        int w=getFirstNeighbor(i);

        //如果下一个节点w存在
        while (w!=-1){
            //如果w节点没有被访问过，则将w看做初始节点进行递归，重复执行1，2,3步骤
            if(!isVisited[w]){
                dfs(isVisited,w); //体现深度优先
            }
            //如果w节点被访问过
            w=getNextNeighbor(i,w);
        }
    }

    /**
     * 深度优先算法原理：
     * 先找到一个初始节点V，并查找到他的邻接节点W，如果能找到邻接节点W，则继续以W为初始节点，继续找以W为初始节点的邻接节点
     * 深度有限遍历算法步骤
     * 1) 访问初始节点v,并标记节点b已访问
     * 2) 查找节点v的第一个邻节点w
     * 3) 若w存在，则想继续执行步骤4，如果w不存在，则回到第1步，将从v的下一个节点继续
     * 4) 若w未被访问，对w进行深度优先遍历递归（即把w当做v，继续执行123步骤）
     * 5) 查找节点v的w邻节点的下一个邻接节点，转到步骤3
     */
    public void dfs(){
        isVisit=new boolean[5];
        for(int j=0;j<vertexList.size();j++){
            if(!isVisit[j]){
                dfs(isVisit,j);
            }
        }
    }




    public static void main(String[] args) {
        String vertexVal[] ={"A","B","C","D","E"};
        //创建图对象
        Graph graph=new Graph(vertexVal.length);

        //循环添加顶点
        for(String vertex:vertexVal){
            graph.insertVertex(vertex);
        }
        //添加边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);


        //显示邻接矩阵
        graph.showGraph();

        //深度优先遍历算法
        System.out.println("----------深度优先遍历算法-----------");
        graph.dfs();
        System.out.println();
        System.out.println("----------广度优先遍历算法-----------");
        graph.bfs();

    }

}